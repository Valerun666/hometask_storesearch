//
//  GradientView.m
//  StoreSearch
//
//  Created by Valerun on 21.04.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "GradientView.h"

@implementation GradientView

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        self.backgroundColor = [UIColor clearColor];
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    }
    
    return self;
}


- (void)drawRect:(CGRect)rect {
    const CGFloat components[8] = { 0.0, 0.0, 0.0, 0.3,
                                    0.0, 0.0, 0.0, 0.7 };
    const CGFloat locations[2] = { 0.0, 1.0 };
    
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(space, components, locations, 2);
    CGColorSpaceRelease(space);
    
    CGFloat x = CGRectGetMidX(self.bounds);
    CGFloat y = CGRectGetMidY(self.bounds);
    CGPoint point = CGPointMake(x, y);
    CGFloat radius = MAX(x, y);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawRadialGradient(context, gradient, point, 0, point, radius, kCGGradientDrawsAfterEndLocation);
    
    CGGradientRelease(gradient);
}

- (void)dealloc
{
    NSLog(@"*** dealloc %@", self);
}

@end
