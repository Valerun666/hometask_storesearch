//
//  LandscapeViewController.m
//  StoreSearch
//
//  Created by Valerun on 22.04.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "LandscapeViewController.h"
#import "SearchResult.h"
#import <AFNetworking/UIButton+AFNetworking.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "Search.h"
#import "DetailViewController.h"

@interface LandscapeViewController () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end

@implementation LandscapeViewController
{
    BOOL _firsTime;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _firsTime = YES;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"LandscapeBackground"]];
    self.pageControl.numberOfPages = 0;
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if (_firsTime) {
        _firsTime = NO;
        
        if (self.search != nil) {
            if (self.search.isLoading) {
                [self showSpinner];
            } else if ([self.search.searchResults count] == 0) {
                [self showNothinFoundLabel];
            } else {
                [self tileButtons];
            }
        }
    }
}

- (void)searchResultsReceived
{
    [self hideSpinner];
    
    if ([self.search.searchResults count] == 0) {
        [self showNothinFoundLabel];
    } else {
        [self tileButtons];
    }
}

- (void)showSpinner
{
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    spinner.center = CGPointMake(CGRectGetMidX(self.scrollView.bounds) + 0.5, CGRectGetMidY(self.scrollView.bounds) + 0.5);
    spinner.tag = 1000;
    [self.view addSubview:spinner];
    [spinner startAnimating];
}

- (void)hideSpinner
{
    [[self.view viewWithTag:1000] removeFromSuperview];
}

- (void)showNothinFoundLabel
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    
    label.text = NSLocalizedString(@"Nothing Found", @"Localized kind: Nothing Found");
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    
    [label sizeToFit];
    CGRect rect = label.frame;
    rect.size.width = ceil(rect.size.width/2.0) * 2.0;
    rect.size.height = ceil(rect.size.height/2.0) * 2.0;
    label.frame = rect;
    label.center = CGPointMake(CGRectGetMidX(self.scrollView.bounds), CGRectGetMidY(self.scrollView.bounds));
    
    [self.view addSubview:label];
}

- (void)tileButtons
{
    int columnsPerPage = 5;
    CGFloat itemWidth = 96.0;
    CGFloat x = 0.0;
    CGFloat extraSpace = 0.0;
    
    CGFloat scrollViewWidth = self.scrollView.bounds.size.width;
    if (scrollViewWidth > 480.0) {
        columnsPerPage = 6;
        itemWidth = 94.0;
        x = 2.0;
        extraSpace = 4.0;
    }
    
    const CGFloat itemHeight = 88.0;
    const CGFloat buttonWidth = 82.0;
    const CGFloat buttonHeight = 82.0;
    const CGFloat marginHorz = (itemWidth - buttonHeight) / 2.0;
    const CGFloat marginVert = (itemHeight - buttonHeight) / 2.0;
    
    int index = 0;
    int row = 0;
    int column = 0;
    
    for (SearchResult *searchResult in self.search.searchResults) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        button.tag = 2000 + index;
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [button setBackgroundImage:[UIImage imageNamed:@"LandscapeButton"] forState:UIControlStateNormal];
//        [button setTitle:[NSString stringWithFormat:@"%d", index] forState:UIControlStateNormal];
        button.frame = CGRectMake(x + marginHorz, 20.0 + row * itemHeight + marginVert, buttonWidth, buttonHeight);
        [self downloadImageForSearchResult:searchResult andPlaceOnButton:button];
        
        [self.scrollView addSubview:button];
        
        index++;
        row++;
        
        if (row == 3) {
            row = 0;
            column++;
            x += itemWidth;
            
            if (column == columnsPerPage) {
                column = 0;
                x += extraSpace;
            }
        }
    }
    
    int titlePerPage = columnsPerPage * 3;
    int numPages = ceil([self.search.searchResults count] / (float)titlePerPage);
    
    self.scrollView.contentSize = CGSizeMake(numPages * scrollViewWidth, self.scrollView.bounds.size.height);
    
    self.pageControl.numberOfPages = numPages;
    self.pageControl.currentPage = 0;
}

- (void)buttonPressed:(UIButton *)sender
{
    DetailViewController *controller = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    
    SearchResult *searchResult = self.search.searchResults[sender.tag - 2000];
    controller.searchResult = searchResult;
    [controller presentInParentViewController:self];
}

- (void)downloadImageForSearchResult:(SearchResult *)searchResult andPlaceOnButton:(UIButton *)button
{
    NSURL *url = [NSURL URLWithString:searchResult.artworkURL60];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request addValue:@"Image/*" forHTTPHeaderField:@"Accept"];
    
    __weak UIButton *weakButton = button;
    
    [button setImageForState:UIControlStateNormal withURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        UIImage *newImage = [self resizeImage:image];
        
        UIImage *unscaledImage = [UIImage imageWithCGImage:newImage.CGImage scale:1.0 orientation:newImage.imageOrientation];
        
        [weakButton setImage:unscaledImage forState:UIControlStateNormal];
    } failure:nil];
}

- (UIImage *)resizeImage:(UIImage *)image
{
    CGFloat horizontelRatio = 60.0 / image.size.width;
    CGFloat verticalRatio =  60.0 / image.size.height;
    CGFloat ratio = MIN(horizontelRatio, verticalRatio);
    CGSize newSize = CGSizeMake(image.size.width * ratio, image.size.height * ratio);
    
    UIGraphicsBeginImageContextWithOptions(newSize, YES, 1.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (IBAction)pageChanged:(UIPageControl *)sender
{
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
        self.scrollView.contentOffset = CGPointMake(self.scrollView.bounds.size.width * sender.currentPage, 0);
    } completion:nil];
}

//!!!!
- (void)dealloc
{
    for (id button in self.scrollView.subviews) {
        if ([button isKindOfClass:[UIButton class]]) {
            [[(UIButton *)button imageView] cancelImageRequestOperation];
        }
    }
}

#pragma - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat width = self.scrollView.bounds.size.width;
    int currentPage = (self.scrollView.contentOffset.x + width / 2.0) / width;
    self.pageControl.currentPage = currentPage;
}

@end
