//
//  SearchResultCell.m
//  StoreSearch
//
//  Created by Valerun on 16.04.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "SearchResultCell.h"
#import "SearchResult.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@implementation SearchResultCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UIView *selectedView = [[UIView alloc] initWithFrame:CGRectZero];
    selectedView.backgroundColor = [UIColor colorWithRed:20/255.0 green:160/255.0 blue:160/255.0 alpha:1.0];
    self.selectedBackgroundView = selectedView;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureForSearchResult:(SearchResult *)searchResult
{
    self.nameLabel.text = searchResult.name;
    
    NSString *artistName = searchResult.artistName;
    if (artistName == nil) {
        artistName = NSLocalizedString(@"Unknown", @"Localized kind: Unknown");
    }
    
    NSString *kind = [searchResult kindForDisplay];
    self.artistNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"ARTIST_NAME_LABEL_FORMAT", @"Format for artist name label"), artistName, kind];
    
//    NSLog(@"*** URL :'%@'", searchResult.artworkURL60);
    
    [self.artworkImageView setImageWithURL:[NSURL URLWithString:searchResult.artworkURL60]
                          placeholderImage:[UIImage imageNamed:@"Placeholder"]];
}


- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self.artworkImageView cancelImageRequestOperation];
    self.nameLabel.text = nil;
    self.artistNameLabel.text = nil;
}

@end
