//
//  LandscapeViewController.h
//  StoreSearch
//
//  Created by Valerun on 22.04.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Search;

@interface LandscapeViewController : UIViewController

@property (nonatomic, strong) Search *search;

- (void)searchResultsReceived;

@end
