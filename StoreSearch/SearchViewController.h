//
//  SearchViewController.h
//  StoreSearch
//
//  Created by Valerun on 15.04.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface SearchViewController : UIViewController

@property (nonatomic, weak) DetailViewController *detailViewController;

@end

