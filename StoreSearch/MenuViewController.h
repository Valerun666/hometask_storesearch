//
//  MenuViewController.h
//  StoreSearch
//
//  Created by Valerun on 28.04.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MenuViewController : UITableViewController

@property (nonatomic, weak) DetailViewController *detailViewController;

@end
